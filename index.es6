 const template = require('lodash/template');
 const util = require("h13a-util");

 const packageJson = require("./package.json");
 const packageJsonMain = require("../../package.json");

 const widgetNamespace = packageJsonMain.name.toLowerCase().replace(/-/g, '');
 const widgetName = packageJson.name.toLowerCase().replace(/-/g, '');
 const widgetFullName = `${widgetNamespace}.${widgetName}`;

 require("h13a-strategy3"); // Simple 1+2 Strategy, initialize and then display ... display ... display
 const widgetIntegrationStrategy = $[widgetNamespace].h13astrategy3;

 const Promise = require("bluebird");
 const defaults = require("./defaults.es6");

 const functions = {

   _readFile: function (location, data, done) {

     require.ensure(["h13a-docs"], function (require) {
       const docs = require("h13a-docs");

       let content = "";
       if (location && location.match(/\.ejs$/)) {
         let code = docs.read(location);
         let compiled = template(code);
         content = compiled({
           data: data.data
         });
       }
       else {
         content = docs.read(location)
       }
       done(null, content);
     });

   }
 }

 const integration = $.extend({
   options: defaults,
   _destroy: function () {
     // The public destroy() method cleans up all common data, events, etc. and then delegates out to _destroy() for custom, widget-specific, cleanup.
     console.log(`${this.widgetFullName}/${this.uuid}: destructor executed.`)
   },

   // Step1 Notes
   // * Create UI
   // * Called once at start
   // * Call display manually
   _prepare: function () {
     let type = this.option('type');
     let location = this.option('location');

     if (location) {
       this.contentNode = $(`<div class="card-block html-position"></div>`);
       this.element.append(this.contentNode);
       this.display();
     }

   },

   // Step2 Notes
   // * Display On Screen
   // * This will be called each time options finish changing
   // * Called after _prepare finishes
   // * Called each time options change.
   display: function () {
     let location = this.option('location');
     this._readFile(location, this.option(), (err, content) => {
       content = content.replace(/%%UUID/g, 'id' + util.guid())
       this.contentNode.html(content);
       for (var propertyName in this.option()) {
         let match = propertyName.match(/(^html|^text|^icon)/);
         if (match) {
           let variableType = match[1];
           let propertyValue = this.option(propertyName);
           propertyValue = propertyValue.replace(/%%NAME/g, packageJsonMain.name)
           propertyValue = propertyValue.replace(/%%VERSION/g, packageJsonMain.version)
           let nodes = $(`*[data-variable=${propertyName}]`, this.element);
           nodes.each((index, element) => {
             if (variableType === 'icon') {
               let classes = $(element).attr("class");
               classes = classes.replace(/fa-[a-z][a-z0-9-]+/g, '').trim();
               $(element).attr("class", 'fa-' + propertyValue + ' ' + classes);
             }
             if (variableType === 'text') {
               $(element).text(propertyValue);
             }
             if (variableType === 'html') {
               $(element).html(propertyValue);
             }
           });
         } // if preoperty name match
       }
     });
   },

 }, functions);

 // Install Widget
 $.widget(widgetFullName, widgetIntegrationStrategy, integration);
